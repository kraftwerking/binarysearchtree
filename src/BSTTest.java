import static org.junit.Assert.*;

import org.junit.Test;

public class BSTTest {

	@Test
	public void test() {
		BST bst = new BST();
		assertFalse(bst.find(1));
		
		bst.insert(20);
		assertTrue(bst.find(20));

		bst.insert(25);
		assertTrue(bst.find(25));
		
		bst.insert(15);
		assertTrue(bst.find(15));
		
		bst.insert(10);
		assertTrue(bst.find(10));
		
		bst.insert(18);
		assertTrue(bst.find(18));

		bst.insert(16);
		assertTrue(bst.find(16));
		
		bst.insert(19);
		assertTrue(bst.find(19));
		
		//Boolean res = bst.delete(20);
		//assertTrue(res == true);
		
		Boolean res = bst.delete(10);
		assertTrue(res == true);

		
	}

}
