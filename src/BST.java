/**
 * 
 */

/**
 * @author rj
 *
 */
public class BST {
	// Binary Search Tree
	public static Node root;

	public BST() {
		super();
		this.root = null;
		// TODO Auto-generated constructor stub
	}

	private class Node {
		int data;
		Node left;
		Node right;

		public Node(int id) {
			super();
			data = id;
			this.left = null;
			this.right = null;
		}

	}
	// Nodes smaller than root goes to the left of the root and Nodes greater
	// than root goes to the right of the root.

	public boolean find(int i) {
		Node current = root;
		while (current != null) {
			if (current.data == i) {
				return true;
			} else if (current.data < i) {
				current = current.right;
			} else {
				current = current.left;
			}
		}
		return false;
	}

	public void insert(int i) {
		Node newNode = new Node(i);

		if (root == null) {
			root = newNode;
			return;
		}
		Node current = root;
		Node parentNode = null;

		while (true) {
			parentNode = current;
			if (current.data < i) {
				current = current.right;
				if (current == null) {
					parentNode.right = newNode;
					return;
				}
			} else {
				current = current.left;
				if (current == null) {
					parentNode.left = newNode;
					return;
				}
			}
		}

	}

	public boolean delete(int i) {
		Node current = root;
		Node parentNode = root;
		boolean isLeftChild = false;

		//looking for where the node is, and whether it's l or r child
		//keep track of its parent node
		while (current.data != i) {
			parentNode = current;
			if (current.data > i) {
				isLeftChild = true;
				current = current.left;
			} else if (current.data < i) {
				isLeftChild = false;
				current = current.right;
			}
			if (current == null) {
				return false; //we didn't find it
			}

		}
		// if i am here that means we have found the node
		// Case 1: if node to be deleted has no children
		if (current.left == null && current.right == null) {
			if (current == root) {
				root = null;
			}
			if (isLeftChild == true) {
				parentNode.left = null;
			} else {
				parentNode.right = null;
			}
		}
		// Case 2 : if node to be deleted has only one child
		else if (current.right == null) {
			if (current == root) {
				root = current.left;
			} else if (isLeftChild) {
				parentNode.left = current.left;
			} else {
				parentNode.right = current.left;
			}
		} else if (current.left == null) {
			if (current == root) {
				root = current.right;
			} else if (isLeftChild) {
				parentNode.left = current.right;
			} else {
				parentNode.right = current.right;
			}
		} else if (current.left != null && current.right != null) {

			// now we have found the minimum element in the right sub tree
			Node successor = getSuccessor(current);
			if (current == root) {
				root = successor;
			} else if (isLeftChild) {
				parentNode.left = successor;
			} else {
				parentNode.right = successor;
			}
			successor.left = current.left;
		}
		return true;
	}

	public Node getSuccessor(Node deleleNode) {
		Node successsor = null;
		Node successsorParent = null;
		Node current = deleleNode.right;
		while (current != null) {
			successsorParent = successsor;
			successsor = current;
			current = current.left;
		}
		// check if successor has the right child, it cannot have left child for
		// sure
		// if it does have the right child, add it to the left of
		// successorParent.
		// successsorParent
		if (successsor != deleleNode.right) {
			successsorParent.left = successsor.right;
			successsor.right = deleleNode.right;
		}
		return successsor;
	}

	public void display(Node root) {
		if (root != null) {
			display(root.left);
			System.out.print(" " + root.data);
			display(root.right);
		}
	}

	public static void main(String arg[]) {
		BST b = new BST();
		b.insert(3);
		b.insert(8);
		b.insert(1);
		b.insert(4);
		b.insert(6);
		b.insert(2);
		b.insert(10);
		b.insert(9);
		b.insert(20);
		b.insert(25);
		b.insert(15);
		b.insert(16);
		System.out.println("Original Tree : ");
		b.display(b.root);
		System.out.println("");
		System.out.println("Check whether Node with value 4 exists : " + b.find(4));
		System.out.println("Delete Node with no children (2) : " + b.delete(2));
		b.display(root);
		System.out.println("\n Delete Node with one child (4) : " + b.delete(4));
		b.display(root);
		System.out.println("\n Delete Node with Two children (10) : " + b.delete(10));
		b.display(root);
	}

}
